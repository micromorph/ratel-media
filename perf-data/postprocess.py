import os
import sys
import pandas as pd
import re
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib import scale
sns.set_theme(style='darkgrid')

# high contrast color palette from:
# https://personal.sron.nl/~pault/#sec:qualitative
colors = ["#DDAA33", "#BB5566", "#005bb5", "#000000", "#228833"]
# Set your custom color palette
sns.set_palette(sns.color_palette(colors))
sns.set(font_scale=1.2)

EVENTS = {
    # 'TSStep': ,
    'KSPSolve',
    'SNESSolve',
    'SNESFunctionEval',
    'SNESJacobianEval',
    'PCSetUp',
    'MatMult',
}


def normalize_hostname(hostname):
    if re.match(r'crusher\d+', hostname):
        return 'Crusher'
    elif re.match(r'[a-z]\d+n\d+', hostname):
        return 'Summit'
    elif re.match(r'lassen\d+', hostname):
        return 'Lassen'
    elif re.match(r'nid\d+', hostname):
        return 'Perlmutter'
    raise RuntimeError(f"Cannot normalize hostname: {hostname}")


def gpus_per_node(hostname):
    table = dict(Crusher=8, Summit=6, Lassen=4, Perlmutter=4)
    return table.get(hostname, 0)


def num_nodes(mpi, gpn, filename):
    mpi_per_gpu = {
        # Crusher
        'schwarz-q2-t20-r2-l2-89099.out': 2,
        'schwarz-q2-t20-r2-l2-89101.out': 2,
        'schwarz-q2-t20-r2-l2-94011.out': 2,
        # Summit
        'schwarz-q2-t20-r2-l2-1952766.out': 2,
        'schwarz-q2-t20-r2-l2-1952897.out': 2,
        'schwarz-q2-t20-r2-l2-1952767.out': 2,
        # Lassen
        'schwarz-q2-t20-r2-l2-3394323.out': 2,
        'schwarz-q2-t20-r2-l2-3394325.out': 2,
        'schwarz-q2-t20-r2-l2-3394384.out': 2,
    }
    nodes = mpi // (mpi_per_gpu.get(os.path.split(filename)[-1], 1) * gpn)
    return nodes


def get_displacements(ll):
    displacements = []

    for disp in ll:
        displacements.append(
            float(
                disp.strip()
                    .replace('[', '')
                    .replace(
                    ']', '').replace(
                    ',', '')))

    return displacements


def parse_file_content(filename):
    record_list = []
    with open(filename, 'r') as fd:
        for line in fd:
            ll = line.strip().split()
            if line.startswith('Ratel Context:'):
                file_data = {}
                file_data['SNES iterations'] = 0
            elif line.strip().startswith('Hostname'):
                hostname = normalize_hostname(ll[-1])
                file_data['Hostname'] = hostname
            elif line.strip().startswith('Polynomial order'):
                file_data['Order'] = int(ll[-1])
            elif line.strip().startswith('MatType'):
                file_data['MatType'] = ll[-1]
            elif line.strip().startswith('-dm_plex_tps_extent'):
                file_data['Extent'] = ll[1].split(',')[0]
            elif line.strip().startswith('Max displacements'):
                file_data['Displacement'] = get_displacements(ll[2:])
            elif line.strip().startswith('Nonlinear solve'):
                file_data['SNES iterations'] = int(ll[-1])
            elif line.strip().startswith('Total ranks'):
                mpi = int(ll[-1])
                file_data['MPI'] = mpi
                nodes = num_nodes(mpi, gpus_per_node(hostname), filename)
                file_data['Nodes'] = nodes
                file_data['GPU'] = nodes * gpus_per_node(hostname)
            elif 'Global DoFs' in line:
                file_data['Global DoFs'] = int(ll[-1])
            elif 'Computed strain energy' in line:
                file_data['Computed strain energy'] = float(ll[-1])
            elif ll and ll[0].strip() in EVENTS:
                time = float(line[29:39])
                file_data[ll[0].strip()] = time
            if line.startswith("#End of PETSc Option Table entries"):
                record_list.append(file_data)
                file_data = {}

    if file_data:
        print(f"Incomplete records in {filename}; discarding incompletes: {file_data}")
    return record_list


def create_data_frame(files_data):
    df = pd.DataFrame.from_records(files_data)
    pd.set_option('display.expand_frame_repr', False)
    pd.set_option('display.float_format', lambda x: '%.12f' % x)

    return df


def run_alg_perf(filenames):
    # parse files
    elem = 'q2'
    files_data = []
    for filename in filenames:
        files_data += parse_file_content(filename)
        if 'q3' in filename:
            elem = 'q3'
    # create a dataframe
    df = create_data_frame(files_data)
    return df, elem


def run_plot(df, events, plot, output, elem):
    df['MPI per GPU'] = df['MPI'] // df['GPU']
    for event in events:
        df.drop(df.loc[df['Hostname'] == 'Lassen']
                [df['Nodes'] > 1].index, inplace=True)

        if event != 'MatMult':
            df[event] /= df['SNES iterations']

        df['Efficiency (MDoF/s/GPU)'] = df['Global DoFs'] / \
            (df[event] * df['GPU']) * 1e-6

        fig, ax = plt.subplots(figsize=(10, 6), layout='tight')
        lp = sns.lineplot(
            x=event,
            y='Efficiency (MDoF/s/GPU)',
            style='Nodes',
            size='MPI per GPU',
            # palette='colorblind',
            hue='Hostname',
            # markers=True,
            sizes=(5, 6),
            alpha=.9,
            ax=ax,
            data=df,
        )

        # Annotate label points
        xmin_array = [l.get_data()[0].min(initial=np.inf)
                      for l in lp.get_lines()]
        xmax_array = [l.get_data()[0].max(initial=-np.inf)
                      for l in lp.get_lines()]
        ymax = np.max([l.get_data()[1].max(initial=-np.inf)
                      for l in lp.get_lines()])
        hostnames = df['Hostname'].unique()

        for h, hostname in enumerate(hostnames):
            dofs = 0
            yval = 0
            for x in xmin_array[:7]:
                df_temp = df[df['Hostname'] == hostname]
                index = df_temp[df[event] == x].index
                if len(index) > 0:
                    dofs = df_temp['Global DoFs'][index].values[0]
                    yval = df_temp['Efficiency (MDoF/s/GPU)'][index].values[0]
                    plt.annotate('{}'.format(dofs // 1000000),
                                 (x * 0.95, yval - 0.04 * ymax))
                    plt.plot(x, yval, color=colors[h], marker='o')
            dofs = 0
            yval = 0
            for x in xmax_array[:7]:
                df_temp = df[df['Hostname'] == hostname]
                index = df_temp[df[event] == x].index
                if len(index) > 0:
                    dofs = df_temp['Global DoFs'][index].values[0]
                    yval = df_temp['Efficiency (MDoF/s/GPU)'][index].values[0]
                    plt.annotate('{} MDoF'.format(dofs // 1000000), (x, yval))
                    plt.plot(
                        x,
                        yval,
                        color=colors[h],
                        marker='o',
                        markersize=5.25)

        xmin = np.min(xmin_array)
        xmax = np.max(xmax_array)
        ax.set_xscale(scale.FuncScale(ax, (np.sqrt, lambda x: x * x)))
        ax.set_xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60])
        ax.set_xlim(left=xmin * 0.8, right=xmax * 1.1)
        ax.set_ylim(bottom=0)
        sns.despine()

        title = '{} efficiency for {} elements'.format(
            {'SNESSolve': 'Nonlinear solve',
             'SNESFunctionEval': 'Nonlinear residual evaluation',
             'SNESJacobianEval': 'Nonlinear Jacobian evaluation',
             'KSPSolve': 'Linear solve',
             'PCSetUp': 'Preconditioner setup',
             'MatMult': 'Matrix multiply',
             }[event],
            {'q2': r'$Q_2$', 'q3': r'$Q_3$'}[elem],
        )
        ax.set_title(title)
        ax.set_xlabel("Time (seconds)")
        legend = plt.legend(loc='lower right', markerscale=3)
        if output:
            plt.savefig(f"{output}-{event}.svg")
            plt.savefig(
                f"../figures/{output}-{event}.pdf",
                bbox_inches='tight')
        if plot:
            plt.show()


def run_plot_apply(df, plot, output):
    fig, ax = plt.subplots(figsize=(10, 6), layout='tight')
    df['Efficiency (GDoF/s/GPU)'] = df['Global DoFs'] / (df['MatMult']
                                                         * df['GPU']) * 1500 * 1e-9  # 1500 matrix multiplications
    df['MatMult (ms)'] = df['MatMult'] * 1000 / 1500
    sns.lineplot(
        #x='MatMult (ms)',
        x='Global DoFs',
        y='Efficiency (GDoF/s/GPU)',
        hue='Order',
        palette=sns.color_palette(colors),
        style='MatType',
        # markers=True,
        size="Nodes",
        sizes=(5, 6),
        alpha=.7,
        ax=ax,
        data=df,
    )
    ax.set_title('Operator Application Efficiency')
    ax.set_xscale('log')
    ax.set_ylim(bottom=0)
    plt.legend(loc='lower right')
    if output:
        plt.savefig(f"{output}-apply.svg")
        plt.savefig(f"../figures/{output}-apply.pdf", bbox_inches='tight')

    if plot:
        plt.show()


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--plot", help="Plot results", action="store_true")
    parser.add_argument(
        "--plot-apply",
        help="Plot operator apply",
        action="store_true")
    parser.add_argument('--event', help="PETSc event to plot", default=None)
    parser.add_argument(
        '--save_data',
        help="Where to save csv to",
        default=None)
    parser.add_argument("--output", help='Output file for plotting')
    parser.add_argument("--filenames", help="List of files", nargs="*")
    args = parser.parse_args()
    df, elem = run_alg_perf(args.filenames)
    print(df)
    if args.plot or args.output:
        events = args.event.split(',') if args.event else EVENTS
        run_plot(df, events, args.plot, args.output, elem)
    if args.plot_apply:
        run_plot_apply(df, args.plot, args.output)
